﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Janken
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// 手の表示名定義
        /// </summary>
        private string[] HandsName = new string[] { "グー", "チョキ", "パー" };

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            // グーボタン
            MatchWithComputer(0);
        }

        private void button2_Click(object sender, EventArgs e) {
            // チョキボタン
            MatchWithComputer(1);
        }

        private void button3_Click(object sender, EventArgs e) {
            // パーボタン
            MatchWithComputer(2);
        }

        private void button4_Click(object sender, EventArgs e)
        {//もう一度ボタン
            //初期化呼び出し
            Form1_Load(this,EventArgs.Empty);
        }

        private void Form1_Load(object sender, EventArgs e)
        {//フォーム呼び出し前に、実行する初期化
            lblPhaseMessage.Text = "じゃんけん…";//かけ声
            lblRivalHand.Text = "";//PC
            lblYourHand.Text = "";//あなた
            label4.Text = "";//結果

            this.SetHandButtonsEnabled(true);
            btnRetry.Enabled = false;//もう一度
        }

        /// <summary>
        /// コンピュータとじゃんけん対戦する
        /// </summary>
        /// <param name="playerHand">プレイヤーの手</param>
        private void MatchWithComputer(int playerHand) {
            int computerHand = new Random().Next(3);

            lblYourHand.Text = HandsName[playerHand];
            lblRivalHand.Text = HandsName[computerHand];

            int result = (computerHand　- playerHand + 3) % 3;

            if (result == 0) {
                // 0の場合は「あいこ」
                lblPhaseMessage.Text = "あいこで";
                return;
            }

            if (result == 1) {
                // 1の場合は「勝ち」
                label4.Text = "勝ちました！";
            } else if(result == 2) {
                // 2の場合は「負け」
                label4.Text = "負けました…";
            }

            // あいこ以外の場合、各所の表示を制御
            if (lblPhaseMessage.Text == "じゃんけん…")
                lblPhaseMessage.Text = "ぽん！";
            if (lblPhaseMessage.Text == "あいこで")
                lblPhaseMessage.Text = "しょ！";
            this.SetHandButtonsEnabled(false);
            btnRetry.Enabled = true;//もう一度
        }

        /// <summary>
        /// 手指定ボタンの活性制御を行います。
        /// </summary>
        /// <param name="enabled">true:活性、false:非活性</param>
        private void SetHandButtonsEnabled(bool enabled) {
            btnHandG.Enabled = enabled;
            btnHandC.Enabled = enabled;
            btnHandP.Enabled = enabled;
        }
    }
}
