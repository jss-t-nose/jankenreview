﻿namespace Janken
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPhaseMessage = new System.Windows.Forms.Label();
            this.lblRivalHand = new System.Windows.Forms.Label();
            this.lblYourHand = new System.Windows.Forms.Label();
            this.btnHandG = new System.Windows.Forms.Button();
            this.btnHandC = new System.Windows.Forms.Button();
            this.btnHandP = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnRetry = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPhaseMessage
            // 
            this.lblPhaseMessage.AutoSize = true;
            this.lblPhaseMessage.Location = new System.Drawing.Point(129, 31);
            this.lblPhaseMessage.Name = "lblPhaseMessage";
            this.lblPhaseMessage.Size = new System.Drawing.Size(35, 12);
            this.lblPhaseMessage.TabIndex = 0;
            this.lblPhaseMessage.Text = "label1";
            // 
            // lblRivalHand
            // 
            this.lblRivalHand.AutoSize = true;
            this.lblRivalHand.Location = new System.Drawing.Point(129, 75);
            this.lblRivalHand.Name = "lblRivalHand";
            this.lblRivalHand.Size = new System.Drawing.Size(35, 12);
            this.lblRivalHand.TabIndex = 1;
            this.lblRivalHand.Text = "label2";
            // 
            // lblYourHand
            // 
            this.lblYourHand.AutoSize = true;
            this.lblYourHand.Location = new System.Drawing.Point(129, 129);
            this.lblYourHand.Name = "lblYourHand";
            this.lblYourHand.Size = new System.Drawing.Size(35, 12);
            this.lblYourHand.TabIndex = 2;
            this.lblYourHand.Text = "label3";
            // 
            // btnHandG
            // 
            this.btnHandG.Location = new System.Drawing.Point(30, 234);
            this.btnHandG.Name = "btnHandG";
            this.btnHandG.Size = new System.Drawing.Size(122, 48);
            this.btnHandG.TabIndex = 3;
            this.btnHandG.Text = "グー";
            this.btnHandG.UseVisualStyleBackColor = true;
            this.btnHandG.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnHandC
            // 
            this.btnHandC.Location = new System.Drawing.Point(176, 234);
            this.btnHandC.Name = "btnHandC";
            this.btnHandC.Size = new System.Drawing.Size(122, 48);
            this.btnHandC.TabIndex = 4;
            this.btnHandC.Text = "チョキ";
            this.btnHandC.UseVisualStyleBackColor = true;
            this.btnHandC.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnHandP
            // 
            this.btnHandP.Location = new System.Drawing.Point(316, 234);
            this.btnHandP.Name = "btnHandP";
            this.btnHandP.Size = new System.Drawing.Size(122, 48);
            this.btnHandP.TabIndex = 5;
            this.btnHandP.Text = "パー";
            this.btnHandP.UseVisualStyleBackColor = true;
            this.btnHandP.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(660, 320);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "label4";
            // 
            // btnRetry
            // 
            this.btnRetry.Location = new System.Drawing.Point(185, 332);
            this.btnRetry.Name = "btnRetry";
            this.btnRetry.Size = new System.Drawing.Size(102, 47);
            this.btnRetry.TabIndex = 7;
            this.btnRetry.Text = "もう一度";
            this.btnRetry.UseVisualStyleBackColor = true;
            this.btnRetry.Click += new System.EventHandler(this.button4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "PC";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "あなた";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnRetry);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnHandP);
            this.Controls.Add(this.btnHandC);
            this.Controls.Add(this.btnHandG);
            this.Controls.Add(this.lblYourHand);
            this.Controls.Add(this.lblRivalHand);
            this.Controls.Add(this.lblPhaseMessage);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPhaseMessage;
        private System.Windows.Forms.Label lblRivalHand;
        private System.Windows.Forms.Label lblYourHand;
        private System.Windows.Forms.Button btnHandG;
        private System.Windows.Forms.Button btnHandC;
        private System.Windows.Forms.Button btnHandP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRetry;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

